﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class UIButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler
{
    public delegate void ClickAction();
    public ClickAction clickAction;

    [SerializeField] Image buttonBackImage;
    [SerializeField] Image buttonFrontImage;
    public Text buttonText;
    RectTransform rect;
    float animTime = 0.1f;
    string buttonID;
    Vector2 currentSize;


   void Awake()
    {
        buttonID = gameObject.GetInstanceID().ToString();
        rect = this.GetComponent<RectTransform>();
    }

    public void OnEnable()
    {
        currentSize = Vector2.one;
        rect.localScale = currentSize;
    }

    void DoAction()
    {
        if (clickAction != null)
            clickAction();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        currentSize = Vector2.one * 1.1f;
        DoSizeAnimation(currentSize);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        currentSize = Vector2.one;
        DoSizeAnimation(currentSize);
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        Vector2 pressed = Vector2.one*0.9f;
        DoSizeAnimation(pressed);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        DoAction();
        DoSizeAnimation(currentSize);
    }

    Tween DoSizeAnimation(Vector2 targetSize)
    {
        DOTween.Kill(buttonID);
        Tween tween = rect.DOScale(targetSize, animTime)
            .SetId(buttonID)
            .SetUpdate(UpdateType.Late, true); ;
        return tween;
    }

    
    void ClickAnimation()
    {
        Vector2 pressedSize = Vector2.one * 0.9f;
        DoSizeAnimation(pressedSize)
            .SetUpdate(UpdateType.Late, true)
            .OnComplete(()=> 
            {
                DoSizeAnimation(currentSize)
                    .SetUpdate(UpdateType.Late, true)
                    .OnComplete(() => DoAction());
            });
    }

}
