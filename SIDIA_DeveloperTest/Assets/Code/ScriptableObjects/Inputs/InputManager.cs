﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControllerType
{
    keyboard,
    mouse
}

[CreateAssetMenu(fileName = "InputManager", menuName = "ScriptableObjects/Input/InputManager")]
public class InputManager : ScriptableObject
{
    [SerializeField] KeyboardInput keyboardInput;
    [SerializeField] MouseInput mouseInput;

    public IInputController GetController(ControllerType type)
    {
        IInputController controller;
        switch (type)
        {
            case ControllerType.keyboard:
                controller = keyboardInput as IInputController;
                break;
            case ControllerType.mouse:
                controller = mouseInput as IInputController;
                break;
            default:
                controller = keyboardInput as IInputController;
                break;
        }
        return controller;
    }
}
