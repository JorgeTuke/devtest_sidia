﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public enum MouseControllerAction
{
    Click,
    SwipeRight,
    SwipeLeft,
    SwipeDown,
    SwipeUp,
    Pressed,
    None
}

[CreateAssetMenu(fileName = "MouseInput", menuName = "ScriptableObjects/Input/MouseInput")]
public class MouseInput : ScriptableObject, IInputController
{
    [SerializeField] float clickTime;
    [SerializeField] float swipeTime;
    [SerializeField] float swipeDeadDistance;
    float clickTimer;
    Vector2 beginPosition;
    Vector2 currentPosition;
    MouseControllerAction action;

    /// <summary>
    /// Get a generic touch input and converts it to a player`s action
    /// </summary>
    /// <returns></returns>
    public PlayerInput GetPlayerInput()
    {
        MouseControllerAction mouseInput = GetMouseInput();
        PlayerInput playerInput = TranslateInput(mouseInput);
        return playerInput;
    }


    /// <summary>
    /// Convert a touch input to some player`s action
    /// </summary>
    /// <param name="touchInput"></param>
    /// <returns></returns>
    PlayerInput TranslateInput(MouseControllerAction mouseInput)
    {
        PlayerInput playerInput;
        switch (mouseInput)
        {
            case MouseControllerAction.Click:
                playerInput = PlayerInput.Jump;
                break;
            case MouseControllerAction.SwipeRight:
                playerInput = PlayerInput.MoveRight;
                break;
            case MouseControllerAction.SwipeLeft:
                playerInput = PlayerInput.MoveLeft;
                break;
            case MouseControllerAction.Pressed:
                playerInput = PlayerInput.StartNinjaRunning;
                break;
            case MouseControllerAction.SwipeDown:
                playerInput = PlayerInput.Slide;
                break;
            default:
                playerInput = PlayerInput.None;
                break;
        }

        return playerInput;
    }

    
    /// <summary>
    /// Recognize touch and gestures
    /// </summary>
    /// <returns></returns>
    MouseControllerAction GetMouseInput()
    {
        action = MouseControllerAction.None;

        if (Input.GetMouseButtonDown(0))
        {
            InitClick(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            ActualizeTouch(Input.mousePosition);
            CheckPressed();
        }

        if (Input.GetMouseButtonUp(0))
        {
            CheckGetSwipeGesture();
            CheckClick();
        }

        
        return action;
    }


    /// <summary>
    /// Prepare to recognize a new touch input
    /// </summary>
    /// <param name="pos">Current touch position</param>
    void InitClick(Vector2 pos)
    {
        beginPosition = pos;
        currentPosition = pos;
        clickTimer = 0;
    }


    /// <summary>
    /// Set the actual touch`s position and how loger it is
    /// </summary>
    /// <param name="pos">Current touch position</param>
    void ActualizeTouch(Vector2 pos)
    {
        currentPosition = pos;
        clickTimer += Time.deltaTime;
    }

    
    /// <summary>
    /// Recognize if the touch remains actived and it`s not a swipe
    /// </summary>
    /// <returns></returns>
    void CheckPressed()
    {
        Vector2 dir = currentPosition - beginPosition;
        bool swipWasNotActived = dir.sqrMagnitude < swipeDeadDistance && clickTimer > clickTime;
        if (swipWasNotActived)
        {
            Debug.Log("Pressed");
            action = MouseControllerAction.Pressed;
        }
    }


    /// <summary>
    /// Recognize if it is a tap gesture
    /// </summary>
    void CheckClick()
    {
        if (clickTimer < clickTime)
        {
            Debug.Log(clickTimer);
            action = MouseControllerAction.Click;
        }
    }


    /// <summary>
    /// Recognize if it is a swipe gesture
    /// </summary>
    void CheckGetSwipeGesture()
    {
        Vector2 dir = currentPosition - beginPosition;
       
        bool swipActived = dir.sqrMagnitude > swipeDeadDistance && clickTimer < swipeTime;
        
        if (swipActived)
        {
            bool horizontalMove = Mathf.Abs(dir.x) > Mathf.Abs(dir.y);
            if (horizontalMove)
            {
                if (dir.x > 0)
                    action = MouseControllerAction.SwipeRight;
                else
                    action = MouseControllerAction.SwipeLeft;
            }
            else
            {
                if (dir.y > 0)
                    action = MouseControllerAction.SwipeUp;
                else
                    action = MouseControllerAction.SwipeDown;
            }
        }

    }


    



}