﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "KeyboardInput", menuName = "ScriptableObjects/Input/KeyboardInput")]
public class KeyboardInput : ScriptableObject, IInputController
{
    [SerializeField] private KeyCode MoveRight;
    [SerializeField] private KeyCode MoveLeft;
    [SerializeField] private KeyCode Slide;
    [SerializeField] private KeyCode Jump;
    [SerializeField] private KeyCode NinjaRun;

    public PlayerInput GetPlayerInput()
    {
        PlayerInput playerInput = PlayerInput.None;

        if (Input.GetKeyDown(Jump))
            playerInput = PlayerInput.Jump;

        if (Input.GetKeyDown(Slide))
            playerInput = PlayerInput.Slide;

        if (Input.GetKey(NinjaRun))
            playerInput = PlayerInput.StartNinjaRunning;

        if (Input.GetKeyDown(MoveRight))
            playerInput = PlayerInput.MoveRight;

        if (Input.GetKeyDown(MoveLeft))
            playerInput = PlayerInput.MoveLeft;

        return playerInput;
    }


}
