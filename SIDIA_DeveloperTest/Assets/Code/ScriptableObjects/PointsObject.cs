﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PointsObject", menuName = "ScriptableObjects/Data/PointsObject")]
public class PointsObject : ScriptableObject
{
    public PlayerData playerData;
    public int value;
    
    public void AddPoints()
    {
        playerData.AddPoints(value);
    }
}
