﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PlayerState
{
    Idle,
    Run,
    NinjaRun,
    FrontFlip,
    BTwist,
    Slide,
    Jump,
    Dead
}


[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/Data/PlayerData", order = 1)]
public class PlayerData : ScriptableObject
{
    [Header("Controller Settings")]
    public InputManager inputManager;
    public ControllerType controllerType;
    public bool controllerEnabled;

    [Header("Move Settings")]
    [Tooltip("How long it takes to reach the right/left position")]
    public float sideMoveTime;
    public float normalSpeed; //10
    public float slidingSpeed; //13 
    public float fastSpeed; //17
    public float jumpSpeed; //10

    [Header("Recovery Time")]
    public float slideTime = 0.8f;
    public float jumpRecoveryTime = 0.8f;
      
    public delegate void Action();
    public Action OnRunStart;
    public Action OnRunEnd;
    public Action OnNinjaRunStart;
    public Action OnNinjaRunEnd;
    public Action OnFrontFlipStart;
    public Action OnFrontFlipEnd;
    public Action OnSlideStart;
    public Action OnSlideEnd;
    public Action OnJumpStart;
    public Action OnJumpEnd;
    public Action OnMoveLeft;
    public Action OnMoveRight;
    public Action OnDead;
    public Action OnReset;
    public Action OnChangePoints;

    [HideInInspector] public int points;
    [HideInInspector] public PlayerState state;


    /// <summary>
    /// Used this to avoid call null callbacks
    /// </summary>
    void CallAction(Action action)
    {
        if (action != null)
            action();
    }


    public IInputController GetController()
    {
        return inputManager.GetController(controllerType);
    }


    public void Reset()
    {
        RemovePoints(points);
        CallAction(OnChangePoints);
        state = PlayerState.Idle;
        CallAction(OnReset);
    }


    public void AddPoints(int value)
    {
        points += value;
        CallAction(OnChangePoints);
    }


    public void RemovePoints(int value)
    {
        points -= value;
        CallAction(OnChangePoints);
    }


    public void SideMove(Vector3 direction)
    {
        if (direction == Vector3.left)
            OnMoveLeft();
        else
            OnMoveRight();
    }


    public void SetState(PlayerState newState)
    {
        switch (state)
        {
            case PlayerState.Run:
                CallAction(OnRunEnd);
                break;
            case PlayerState.NinjaRun:
                CallAction(OnNinjaRunEnd);
                break;
            case PlayerState.FrontFlip:
                CallAction(OnFrontFlipEnd);
                break;
            case PlayerState.Slide:
                CallAction(OnSlideEnd);
                break;
            case PlayerState.Jump:
                CallAction(OnJumpEnd);
                break;
        }


        switch (newState)
        {
            case PlayerState.Run:
                CallAction(OnRunStart);
                break;
            case PlayerState.NinjaRun:
                CallAction(OnNinjaRunStart);
                break;
            case PlayerState.FrontFlip:
                CallAction(OnFrontFlipStart);
                break;
           case PlayerState.Slide:
                CallAction(OnSlideStart);
                break;
            case PlayerState.Jump:
                CallAction(OnJumpStart);
                break;
            case PlayerState.Dead:
                CallAction(OnDead);
                break;
        }

        state = newState;
    }

}
