﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "KunaiWallSettings", menuName = "ScriptableObjects/Traps/KunaiWallSettings")]
public class KunaiWallSettings : ScriptableObject
{
    [Tooltip("How longer the kunai will travel")]
    public float range;
    [Tooltip("The kunai velocity")]
    public float kunaiVelocity;
}
