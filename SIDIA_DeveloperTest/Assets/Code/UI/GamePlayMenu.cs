﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GamePlayMenu : Panel
{
    [SerializeField] Panel mainMenu;
    [SerializeField] Popup popup;
    [SerializeField] PlayerData playerData;
    [SerializeField] Text points;
    [SerializeField] UIButton btPause;
    bool isPaused;

   
    private void Start()
    {
        btPause.clickAction = PauseGame;
        WritePoints();
        playerData.OnChangePoints += WritePoints;
    }


    public void OnEnable()
    {
        isPaused = false;
    }


    /// <summary>
    /// Actualize the player points
    /// </summary>
    void WritePoints()
    {
        points.text = playerData.points.ToString();
    }


    /// <summary>
    /// Pause and call the popup
    /// </summary>
    void PauseGame()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            popup.Show
            (
                "Pause",
                "Quit",
                "Resume",
                QuitGame,
                PauseGame
            );
        }

        Time.timeScale = isPaused? 0 : 1;
        DOTween.timeScale = 1;
    }


    /// <summary>
    /// Back to menu
    /// </summary>
    public void QuitGame()
    {
        mainMenu.Open();
        Time.timeScale = 1;
        base.Close();
    }

}
