﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : Panel
{
    [SerializeField] Panel mainMenu;
    [SerializeField] UIButton btQuit;
    [SerializeField] UIButton btRestart;
    [SerializeField] Text points; 
    [SerializeField] GameManager gameManager;
    [SerializeField] PlayerData playerData;

    void Start()
    {
        btRestart.clickAction = RestarGame;
        btQuit.clickAction = Close;
        WritePoints();
        playerData.OnChangePoints += WritePoints;
    }

    void RestarGame()
    {
        gameManager.Restart();
        base.Close();
    }

    public override void Close()
    {
        mainMenu.Open();
        base.Close();
    }
    
    void WritePoints()
    {
        points.text = playerData.points.ToString();
    }

}
