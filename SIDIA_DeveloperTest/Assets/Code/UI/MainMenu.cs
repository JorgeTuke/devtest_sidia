﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : Panel
{
    [SerializeField] private Popup popup;
    [SerializeField] private GameManager gameManger;
    [SerializeField] private UIButton btStart;
    [SerializeField] private UIButton btQuit;


    void Start()
    {
        btStart.clickAction = StartGame;
        btQuit.clickAction = ConfirmQuitApplication;
    }


    void StartGame()
    {
        gameManger.StartGame();
        base.Close();
    }

    
    void ConfirmQuitApplication()
    {
        popup.Show(
            "Do you really want to quit?", 
            "Yes", 
            "No", 
            QuitApp,
            null
        );
    }


    void QuitApp()
    {
        Debug.Log("QuitApplication");
        Application.Quit();
    }

}
