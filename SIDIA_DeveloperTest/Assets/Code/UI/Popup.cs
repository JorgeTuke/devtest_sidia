﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Popup : MonoBehaviour
{
    public delegate void Callback();
    Callback callbackConfirm;
    Callback callbackCancel;

    [SerializeField] private Image alphaBackground;
    [SerializeField] private RectTransform popupElement;
    [SerializeField] private UIButton confirmButton;
    [SerializeField] private UIButton cancelButton;
    [SerializeField] private Text message;

    [Header("Animation Settings")]
    [SerializeField] private float backgroundFadeTime;
    [SerializeField] private float popupSlideTime;
    [SerializeField] private Vector2 popupHidePosition;


    private void Start()
    {
        //Set buttons` funtions
        confirmButton.clickAction = Confirm;
        cancelButton.clickAction = Hide;
    }


    /// <summary>
    /// Setup and call the popup`s animaton
    /// </summary>
    public void Show(string popupMessage, string confirmMessage, string cancelMessage, Callback callbackConfirm, Callback callbackCancel)
    {
        this.gameObject.SetActive(true);
        message.text = popupMessage;
        confirmButton.buttonText.text = confirmMessage;
        cancelButton.buttonText.text = cancelMessage;
        this.callbackConfirm = callbackConfirm;
        this.callbackCancel = callbackCancel;

        //Force the text field actualize its size!
        Canvas.ForceUpdateCanvases();

        EnterAnimation();
    }


    public void EnterAnimation()
    {
        //Start animation
        Color color = alphaBackground.color;
        color.a = 0;
        alphaBackground.color = color;
        popupElement.anchoredPosition = popupHidePosition;

        //Fade background to black
        alphaBackground.DOFade(0.9f, backgroundFadeTime)
            .SetUpdate(UpdateType.Late, true);

        //Make popup slides into the screen
        popupElement.DOAnchorPos(Vector2.zero, popupSlideTime)
            .SetEase(Ease.InOutBack, 1f)
            .SetUpdate(UpdateType.Late, true); 
    }


    public void Confirm()
    {
        if (callbackConfirm != null)
        {
            callbackConfirm();
            callbackConfirm = null;
        }
            
        this.gameObject.SetActive(false);
    }


    public void Hide()
    {
        if (callbackCancel != null)
        {
            callbackCancel();
            callbackCancel = null;
        }
            
        this.gameObject.SetActive(false);
    }

}
