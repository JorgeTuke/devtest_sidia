﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearTrap : MonoBehaviour
{
    [Header("Build")]
    [SerializeField] public int wallQuantity;
    [SerializeField] public GameObject prefSpearWall;

    [Header("Settings")]
    [SerializeField] private bool InvertSequence;
    [SerializeField] private float ActiveRate;
    [SerializeField] private float attackInterval;
    [SerializeField] private float attackSpeed;

    [SerializeField] public SpearWall[] spearWall;
    private float timer;
    
    
    private void OnEnable()
    {
        for (int n = 0; n < wallQuantity; n++)
        {
            spearWall[n].Setup(attackSpeed);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        ActiveTrap();
    }


    [ContextMenu("CreateWall")]
    void CreateWall()
    {

        spearWall = new SpearWall[wallQuantity];
        float wallSizeX = prefSpearWall.transform.localScale.x;

        Vector3 position;
        Vector3 mirrorRotation = new Vector3(0, 180, 0);

        for (int n = 0; n < wallQuantity; n++)
        {
            
            if (InvertSequence)
                position = transform.position + (-transform.right) * wallSizeX * n;
            else
                position = transform.position + transform.right * wallSizeX * n;

            if (n < transform.childCount)
            {
                spearWall[n] = transform.GetChild(n).GetComponent<SpearWall>();
                spearWall[n].transform.position = position;
                spearWall[n].transform.eulerAngles = transform.eulerAngles;
            }
            else
            {
                spearWall[n] = Instantiate(prefSpearWall, position, transform.rotation).GetComponent<SpearWall>();
            }

            spearWall[n].transform.SetParent(transform);
        }
    }

    [ContextMenu("ActiveTrap")]
    void ActiveTrap()
    {
        for (int n = 0; n < spearWall.Length; n++)
            spearWall[n].ActiveDelay(attackInterval * (n + 1));
    }


    void Update()
    {
        timer += Time.deltaTime;
        if (timer>ActiveRate)
        {
            timer = 0;
            ActiveTrap();
        }
    }
}
