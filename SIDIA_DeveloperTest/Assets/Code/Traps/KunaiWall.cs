﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiWall : MonoBehaviour
{
    [SerializeField] KunaiWallSettings settings;
    [SerializeField] Transform kunai;
    [SerializeField] ParticleSystem smoke;
    private float range;
    private float kunaiVelocity;
    private bool kunaiThrowed;
    private float distanceTraveled;
    private bool waitingDelay;
    private float delayTime;
    private float timer;


    /// <summary>
    /// Load default setting and reset the trap
    /// </summary>
    public void Setup()
    {
        this.kunaiVelocity = settings.kunaiVelocity;
        this.range = settings.range;
        ResetTrap();        
    }


    void ThrowKunai()
    {
        kunaiThrowed = true;
        smoke.Play();
        kunai.gameObject.SetActive(true);
    }

    public void ThrowKunaiDelay(float delayTime)
    {
        ResetTrap();
        this.delayTime = delayTime;
        waitingDelay = true;
    }


    void ResetTrap()
    {
        kunai.gameObject.SetActive(false);
        kunaiThrowed = false;
        waitingDelay = false;
        distanceTraveled = 0;
        delayTime = 0;
        //timer = 0;
        kunai.localPosition = Vector3.zero;
        kunai.localEulerAngles = Vector3.zero;
    }
    


    void Update()
    {
        /*
        if (waitingDelay)
        {
            timer += Time.deltaTime;
            if (timer > delayTime)
            {
                waitingDelay = false;
                ThrowKunai();
            }
        }

        if (kunaiThrowed)
        {
            distanceTraveled += kunaiVelocity * Time.deltaTime;
            kunai.position += kunai.forward * distanceTraveled;
            if (distanceTraveled > range)
            {
                ResetTrap();
            }
        }
        */
        timer += Time.deltaTime;
        if (timer > 0.5f)
        {
            timer = 0;
            ThrowKunai();
        }


        if (kunaiThrowed)
        {
            float velocity = 10f * Time.deltaTime;
            distanceTraveled += velocity;
           
            kunai.position += kunai.forward * velocity;
            kunai.Rotate(Vector3.forward * 50);
            if (distanceTraveled > 10)
            {
                ResetTrap();
            }
        }

    }
    

}
