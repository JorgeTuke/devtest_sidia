﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

[Serializable]
public struct BladeSet
{
    public Transform blade;
    public ParticleSystem smoke;
    [HideInInspector] public bool isShowing;

    public void SwitchState(bool state)
    {
        isShowing = state;
        if (isShowing)
            Show();
        else
            Hide();
    }

    void Show()
    {
        blade.DOLocalMoveY(0, 0.3f);
        smoke.Play();
    }

    void Hide()
    {
        blade.DOLocalMoveY(-2.5f, 0.3f);
    }
}

public class BladeWall : MonoBehaviour
{
    [Header("blades")]
    [SerializeField] BladeSet topBlade;
    [SerializeField] BladeSet botBlade;
    [Header("Settings")]
    [SerializeField] float switchBladesTime;

    float timer;

    private void Start()
    {
        timer = 0;
        topBlade.isShowing = true;
        botBlade.isShowing = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer> switchBladesTime)
        {
            timer = 0;
            topBlade.SwitchState(!topBlade.isShowing);
            botBlade.SwitchState(!botBlade.isShowing);
        }
    }
}
