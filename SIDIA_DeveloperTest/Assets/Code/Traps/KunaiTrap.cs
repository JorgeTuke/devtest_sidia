﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KunaiTrap : MonoBehaviour
{
    [Header("Build")]
    [SerializeField] public int wallQuantity;
    [SerializeField] public GameObject prefKunaiWall;
    [SerializeField] public Transform parentKunaiWall;

    [Header("Settings")]
    [SerializeField] private float throwInterval;

    [HideInInspector] public KunaiWall[] kunaiWall;

    private void OnEnable()
    {
        for (int n = 0; n < wallQuantity; n++)
        {
            kunaiWall[n].Setup();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ActiveKunais();
    }

    [ContextMenu ("CreateWall")]
    void CreateWall()
    {
        kunaiWall = new KunaiWall[wallQuantity];
        float wallSizeX = prefKunaiWall.transform.localScale.x;

        Vector3 position; 
        for (int n=0; n< wallQuantity; n++)
        {
            position = transform.position + transform.right * wallSizeX * n;
            if (n < transform.childCount)
            {
                kunaiWall[n] = parentKunaiWall.GetChild(n).GetComponent<KunaiWall>();
                kunaiWall[n].transform.position = position;
                kunaiWall[n].transform.eulerAngles = transform.eulerAngles;
            }
            else
            {
                kunaiWall[n] = Instantiate(prefKunaiWall, position, transform.rotation).GetComponent<KunaiWall>();
            }
            kunaiWall[n].transform.SetParent(parentKunaiWall);
        }
    }

    [ContextMenu("ActiveTrap")]
    void ActiveKunais()
    {
        for (int n = 0; n < wallQuantity; n++)
        {
            kunaiWall[n].ThrowKunaiDelay( throwInterval*(n+1) );
        }
    }

}
