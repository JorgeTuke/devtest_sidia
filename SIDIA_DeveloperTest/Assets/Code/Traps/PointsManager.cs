﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsManager : MonoBehaviour
{
    GameObject[] childs;

    void Awake()
    {
        childs = new GameObject[transform.childCount];
        for(int n =0; n< childs.Length; n++)
        {
            childs[n] = transform.GetChild(n).gameObject;
        }
    }

    void OnEnable()
    {
        for(int n=0; n< childs.Length; n++)
        {
            childs[n].SetActive(true);
        }
    }

}
