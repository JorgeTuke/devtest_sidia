﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpearWall : MonoBehaviour
{
    [SerializeField] Transform spear;
    [SerializeField] ParticleSystem smoke;
    float attackPos = 2;
    float hidePos = 0;
    float timeAttack = 0.5f;
    float delay;
    float timer;
    bool waitForActive;


    public void Setup(float timeAttack)
    {
        this.timeAttack = timeAttack;
    }


    public void ActiveDelay(float delay)
    {
        this.delay = delay;
        timer = 0;
        waitForActive = true;
    }


    [ContextMenu("Active Trap")]
    public void Active()
    {
        smoke.Play();
        spear.DOLocalMoveY(attackPos, timeAttack).OnComplete(()=> 
        {
            spear.DOLocalMoveY(hidePos, timeAttack);
        });
    }


    void Update()
    {
        if (waitForActive)
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                waitForActive = false;
                timer = 0;
                Active();
            }
        }
    }

}
