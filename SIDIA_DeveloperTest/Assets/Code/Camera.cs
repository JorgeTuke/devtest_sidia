﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Camera : MonoBehaviour
{
    [SerializeField] PlayerData player;
    [SerializeField] Transform closePosition;
    [SerializeField] Transform normalPosition;
    [SerializeField] Transform farPosition;
    [Header("Animation Times")]
    [SerializeField] float sideMoveSmoothly = 10f;
    [SerializeField] float timeSlideIn = 0.8f;
    [SerializeField] float timeSlideOut = 1f;
    [SerializeField] float getBackTime = 0.8f;
    private int tweenID;
    Sequence cameraAnimation;


    void OnEnable()
    {
        tweenID = this.GetInstanceID();
        player.OnRunStart += BackNormal;
        player.OnSlideStart += SlideAnimation;
        player.OnNinjaRunStart += OpenView;
        player.OnNinjaRunEnd += BackNormal;
        player.OnMoveLeft += KillAnimation;
        player.OnMoveRight += KillAnimation;
        cameraAnimation = null;
    }


    private void OnDisable()
    {
        player.OnRunStart -= BackNormal;
        player.OnSlideStart -= SlideAnimation;
        player.OnNinjaRunStart -= OpenView;
        player.OnNinjaRunEnd -= BackNormal;
        player.OnMoveLeft -= KillAnimation;
        player.OnMoveRight -= KillAnimation;
    }


    void LateUpdate()
    {
        if (cameraAnimation == null)
        {
            SideAnimation();
        }
    }

    Sequence SetCamPosition(Transform target, float time)
    {
        DOTween.Kill(tweenID);
        cameraAnimation = DOTween.Sequence();
        cameraAnimation.SetId(tweenID);
        cameraAnimation.Insert(0f, transform.DOLocalMove(target.position, time));
        cameraAnimation.Insert(0f, transform.DORotate(target.eulerAngles, time));
        cameraAnimation.OnComplete(() => cameraAnimation = null);
        return cameraAnimation;
    }


    void BackNormal()
    {
        SetCamPosition(normalPosition, getBackTime);
    }


    void OpenView()
    {
        SetCamPosition(farPosition, getBackTime);
    }


    void SlideAnimation()
    {
        SetCamPosition(closePosition, timeSlideIn).OnComplete(()=> { SetCamPosition(normalPosition, 1f); });
    }

    void SideAnimation()
    {
        Transform target;
        if (player.state == PlayerState.NinjaRun)
            target = farPosition;
        else
            target = normalPosition;
        transform.position = Vector3.Lerp(transform.position, target.position, sideMoveSmoothly * Time.deltaTime);
        transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, target.eulerAngles, sideMoveSmoothly * Time.deltaTime);
    }

    void KillAnimation()
    {
        DOTween.Kill(tweenID);
        cameraAnimation = null;
    }

}
