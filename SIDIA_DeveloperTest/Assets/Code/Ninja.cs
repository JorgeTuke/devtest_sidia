﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;


[Serializable]
public struct StateCollider
{
    public Collider normal;
    public Collider jump;
    public Collider slide;

    /// <summary>
    /// Switch wich collider will be enabled
    /// </summary>
    /// <param name="col">Collider to enable</param>
    public void Enable(Collider col)
    {
        normal.enabled = col == normal ? true : false;
        jump.enabled = col == jump ? true : false;
        slide.enabled = col == slide ? true : false;
    }

}

public enum PlayerInput
{
    Jump,
    MoveRight,
    MoveLeft,
    Slide,
    StartNinjaRunning,
    EndNinjaRunning,
    None
}

public class Ninja : MonoBehaviour
{
    //Callback
    public delegate void Callback();
    public Callback animationCallback;
    //Inspector
    public PlayerData playerData;
    public Transform ninja;
    public Animator animator;
    public ParticleSystem speedFX;
    public Transform slideCam;
    public Transform normalCam;
    public Transform ninjaSpeedCam;
    public StateCollider stateColliders;
    //Script only
    private float finishAnimationTimer;
    private Vector3 actualLayerPosition;
    private IInputController inputController;
    
    void Start()
    {
        playerData.OnReset = Reset;
        finishAnimationTimer = 0;
        stateColliders.Enable(stateColliders.normal);
        inputController = playerData.GetController();
    }

    void Reset()
    {
        finishAnimationTimer = 0;
        stateColliders.Enable(stateColliders.normal);
        animator.SetBool("NinjaStyle", false);
        animator.SetTrigger("Reset");
        actualLayerPosition = Vector3.zero;
        transform.localPosition = actualLayerPosition;
    }

    void Update()
    {
        //Animation timer used for callbacks
        if (IsRunningAnimation())
        {
            WaitingAnimation();
        }

        if (playerData.controllerEnabled)
        {
            PlayerInput currentInput = inputController.GetPlayerInput();

            //Inputs   
            if (playerData.state == PlayerState.Idle && currentInput == PlayerInput.Jump)
            {
                StartRunning();
            }
            else if (playerData.state != PlayerState.Idle)
            {
                if (playerData.state == PlayerState.Run || playerData.state == PlayerState.NinjaRun)
                {

                    if (currentInput == PlayerInput.MoveRight)
                        MoveRight();

                    if (currentInput == PlayerInput.MoveLeft)
                        MoveLeft();

                    if (currentInput == PlayerInput.Slide)
                        SlideAnimation();

                    if (currentInput == PlayerInput.Jump)
                        Jump();

                    if (currentInput == PlayerInput.StartNinjaRunning)
                        StartNinjaRunnning();

                    if (playerData.state == PlayerState.NinjaRun && currentInput == PlayerInput.None)
                        StopNinjaRunning();
                }
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Points") && playerData.state != PlayerState.Dead){
            Die();
        }
    }


    /// <summary>
    /// Start WaitingAnimation()
    /// </summary>
    /// <param name="animationTime">How longer will be waiting for</param>
    /// <param name="callback">Do something after the time`s up</param>
    void WaitForAnimation(float animationTime, Callback callback)
    {
        finishAnimationTimer = animationTime;
        animationCallback = callback;
    }

    
    /// <summary>
    /// Recognize if it`s waiting for some animation`s end
    /// </summary>
    /// <returns>waintig time bigger than zero</returns>
    bool IsRunningAnimation()
    {
        return finishAnimationTimer > 0;
    }


    /// <summary>
    /// Count down to call some method mathcing with animation
    /// </summary>
    void WaitingAnimation()
    {
        finishAnimationTimer -= Time.deltaTime;
        if (finishAnimationTimer < 0)
        {
            if (animationCallback != null)
            {
                animationCallback();
                animationCallback = null;
            }
        }
    }


    /// <summary>
    /// Start the game
    /// </summary>
    void StartRunning()
    {
        animator.SetTrigger("StartRun");
        playerData.SetState(PlayerState.Run);
    }


    /// <summary>
    /// Game over
    /// </summary>
    void Die()
    {
        playerData.SetState(PlayerState.Dead);
        animator.SetTrigger("Die");
        speedFX.Stop();
    }


    /// <summary>
    /// Make the character slide on the ground
    /// </summary>
    void SlideAnimation()
    {
        animator.SetTrigger("Slide");
        playerData.SetState(PlayerState.Slide);
        SetCollider(PlayerState.Slide);
        WaitForAnimation(playerData.slideTime, () => {
            playerData.SetState(PlayerState.Run);
            SetCollider(PlayerState.Run);
        });
    }


    /// <summary>
    /// Jump Action
    /// </summary>
    void Jump()
    {
        animator.SetTrigger("Jump");
        playerData.SetState(PlayerState.Jump);
        SetCollider(PlayerState.Jump);
        WaitForAnimation(playerData.jumpRecoveryTime, () => {
            playerData.SetState(PlayerState.Run);
            SetCollider(PlayerState.Run);
        });
    }


    /// <summary>
    /// Move the character a little bit up when he jumps, matching with animation`s time
    /// </summary>
    void JumpTween()
    {
        ninja.DOMoveY(1, 0.2f).SetEase(Ease.OutExpo)
            .OnComplete(()=> ninja.DOMoveY(0, 0.3f).SetEase(Ease.InOutExpo));
    }


    /// <summary>
    /// Start fast speed runnnin` ability
    /// </summary>
    void StartNinjaRunnning()
    {
        animator.SetBool("NinjaStyle", true);
        playerData.SetState(PlayerState.NinjaRun);
        speedFX.Play();
    }


    /// <summary>
    /// Stop the fast speed runnin` ability
    /// </summary>
    void StopNinjaRunning()
    {
        animator.SetBool("NinjaStyle", false);
        playerData.SetState(PlayerState.Run);
        speedFX.Stop();
    }


    /// <summary>
    /// Move player 1 step to the left
    /// </summary>
    void MoveLeft()
    {
        bool enableLeftMove = actualLayerPosition.x > -1;
        if (enableLeftMove)
        {
            Vector3 targetPostion = actualLayerPosition + Vector3.left;
            actualLayerPosition = targetPostion;
            transform.DOMove(targetPostion, playerData.sideMoveTime);
        }
        playerData.SideMove(Vector3.left);
    }

    
    /// <summary>
    /// Move player 1 step to the right
    /// </summary>
    void MoveRight()
    {
        bool enableRightMove = actualLayerPosition.x < 1;
        if (enableRightMove)
        {
            Vector3 targetPostion = actualLayerPosition + Vector3.right;
            actualLayerPosition = targetPostion;
            transform.DOMove(targetPostion, playerData.sideMoveTime);
        }
        playerData.SideMove(Vector3.right);
    }


    /// <summary>
    /// Change the colliders for determinate actions
    /// </summary>
    /// <param name="state">Actual player state</param>
    void SetCollider(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.Slide:
                stateColliders.Enable(stateColliders.slide);
                break;
            case PlayerState.Jump:
                stateColliders.Enable(stateColliders.jump);
                break;
            default:
                stateColliders.Enable(stateColliders.normal);
                break;
        }
    }

}
