﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    [Header("Screens")]
    [SerializeField] Panel pnGameOver;
    [SerializeField] Panel pnGamePlay;

    public float timeToEnableController;
    public GameObject prefPlataform;
    public int maxPlataforms;
    public int maxPlataformDisabled;
    public List<Transform> plataforms;
    public PlayerData playerData;
    float minZPosition = -20;
    int plataformIndex;
    float plataformSize;
    

    void Start()
    {
        playerData.OnDead = GameOver;
        CreatePlataforms();
        playerData.controllerEnabled = false;
    }


    void Update()
    {
        if (playerData.state != PlayerState.Idle && playerData.state != PlayerState.Dead)
        {
            RollPlataforms();
        }
    }


    public void StartGame()
    {
        playerData.Reset();
        CreatePlataforms();
        pnGamePlay.Open();
        StartCoroutine(EnableController());
    }


    public void Restart()
    {
        playerData.Reset();
        CreatePlataforms();
        pnGamePlay.Open();
        StartCoroutine(EnableController());
    }


    public IEnumerator EnableController()
    {
        yield return new WaitForSeconds(timeToEnableController);
        playerData.controllerEnabled = true;
    }


    void GameOver()
    {
        pnGamePlay.Close();
        pnGameOver.Open();
        playerData.controllerEnabled = false;
    }

    /// <summary>
    /// Set plataforms` position and instantiate new ones if need 
    /// </summary>
    [ContextMenu("Refresh Plataforms")]
    void CreatePlataforms()
    {
        GameObject sceneObjects = GameObject.Find("SceneObjects");
        if (!sceneObjects)
            sceneObjects = new GameObject("SceneObjects");
        
        plataforms = new List<Transform>();
        plataformIndex = 0;

        for (int n=0; n<maxPlataforms; n++)
        {
            if (n < sceneObjects.transform.childCount)
            {
                //Reuse existent plataform
                plataforms.Add(sceneObjects.transform.GetChild(n));
            }
            else
            {
                //Instantiate a new one
                plataforms.Add(Instantiate(prefPlataform, Vector3.zero, Quaternion.identity).transform);
                plataforms[n].SetParent(sceneObjects.transform);
            }
        }

        plataformSize = plataforms[0].GetChild(0).localScale.y;

        //Organize them
        for (int n = 0; n < maxPlataforms; n++)
        { 
            plataforms[n].transform.position = Vector3.forward * plataformSize * n;
            bool enable = n <= (maxPlataforms - maxPlataformDisabled);
            plataforms[n].gameObject.SetActive(enable);
        }
    }


    /// <summary>
    /// Make the plataforms pass through the player
    /// </summary>
    void RollPlataforms()
    {
        //Set roll speed
        float plataformMoveSpeed;
        switch (playerData.state)
        {
           case PlayerState.Slide:
                plataformMoveSpeed = playerData.slidingSpeed;
                break;
            case PlayerState.NinjaRun:
                plataformMoveSpeed = playerData.fastSpeed;
                break;
            case PlayerState.Jump:
                plataformMoveSpeed = playerData.jumpSpeed;
                break;
            case PlayerState.Dead:
                plataformMoveSpeed = 0;
                break;
            default:
                plataformMoveSpeed = playerData.normalSpeed;
                break;
        }

        //Actualize positions
        plataforms[0].transform.position += Vector3.back * plataformMoveSpeed * Time.deltaTime;
        for (int n = 1; n < maxPlataforms; n++)
        {
            Vector3 plataformPos = plataforms[0].transform.position;
            plataformPos.z += plataformSize * n;
            plataforms[n].transform.position = plataformPos;
        }

        //Loop
        if (plataforms[0].transform.position.z < minZPosition)
        {
            Transform aux = plataforms[0];
            plataforms.RemoveAt(0);
            plataforms.Add(aux);
            aux.gameObject.SetActive(false);
            int index = plataforms.Count - maxPlataformDisabled;
            index = index < 0 ? 0 : index;
            plataforms[index].gameObject.SetActive(true);
        }
    }


    

}
