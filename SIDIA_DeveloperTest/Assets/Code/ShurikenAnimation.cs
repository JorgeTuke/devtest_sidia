﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenAnimation : MonoBehaviour
{
    public PointsObject shurikenPoints;
    float rotVelocity;
    
    // Start is called before the first frame update
    void Start()
    {
        rotVelocity = Random.Range(40, 80);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * rotVelocity * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        shurikenPoints.AddPoints();
    }
}
